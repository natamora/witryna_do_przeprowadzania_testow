@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Details</div>
                    </div>
                    <div class="panel-body">

                        @if (Auth::user()->identity == "teacher")
                            <h4><strong>Student:</strong>
                                {{DB::table('users')->where('id', $id)->value('name')}}</h4> <br>
                            <h3><strong>Tests:</strong></h3> <br>
                        @foreach($course->tests as $test)
                                <strong>{{$test->name}} </strong>
                                <p class="text-primary">@if(DB::table('user_points')->where('user_id', $id)->where('test_id', $test->id)->value('finished') == true)
                                    Score: {{DB::table('user_points')->where('user_id', $id)->where('test_id', $test->id)->value('points')}}
                                    / {{$test->questions()->count()}} <br>
                                @else
                                    <i>Not solved</i> <br>
                                    @endif</p>
                        @endforeach

                        @elseif (Auth::user()->identity == "student")
                            404
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
