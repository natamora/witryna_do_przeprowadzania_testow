<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('apply for a course as a student');


$student_name = 'Jane Doe';
$student_email = 'jane@doe.com';
$student_password = 'jane123';

$teacher_name = 'Rasmus Lerdorf';
$teacher_email = 'teacher@mail.com';
$teacher_password = 'rsms123';


$I->haveInDatabase('users', [
    'email' => $student_email,
    'name' => $student_name,
    'password' => password_hash($student_password, PASSWORD_DEFAULT),
    'identity' => 'student',
]);

$I->haveInDatabase('users', [
    'email' => $teacher_email,
    'name' => $teacher_name,
    'password' => password_hash($teacher_password, PASSWORD_DEFAULT),
    'identity' => 'teacher',
]);

$student_id = $I->grabFromDatabase('users', 'id', array('email' => $student_email));
$teacher_id = $I->grabFromDatabase('users', 'id', array('email' => $teacher_email));


$course = 'PHP II';
$description = 'advanced programming with PHP';

$I->haveInDatabase('courses', [
    'name' => $course,
    'description' => $description,
    'teacher_id' => $teacher_id,
]);

$course_id = $I->grabFromDatabase('courses', 'id', array('name'=> $course));



$I->amOnPage('/login');
$I->fillField('email', $student_email);
$I->fillField('password', $student_password);
$I->click('button[type=submit]');

$I->dontSeeInDatabase('course_user',[
    'user_id' => $student_id,
    'course_id' => $course_id,
    'confirmed' => false
]);


$I->amOnPage('/courses');
$I->seeCurrentUrlEquals('/courses');
$I->seeLink('Apply for...');
$I->click('Apply for...');

$I->see('Page to apply for the course', 'div.panel-body');
$I->selectOption('course', $course_id . ' ' . $course . ' ' . $teacher_email);
$I->click('Apply');
$I->see($course, 'h5');


$I->seeInDatabase('course_user',[
    'user_id' => $student_id,
    'course_id' => $course_id,
    'confirmed' => false
]);