@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Courses</div>
                    </div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif



                        @foreach(Auth::user()->courses as $course)
                            <div  class="list-group">
                                <a href="{{ route('courses.show', $course->id) }}" class="list-group-item list-group-item-action flex-column align-items-start">

                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1"><b>{{ $course['name'] }}</b></h5>
                                    </div>
                                    <p  class="text-primary text-nowrap">{{$course['description']}}</p>
                                </a>
                            </div>
                        @endforeach
                        @if (Auth::user()->identity == "teacher")
                            <a href="{{ route('courses.create') }}" class="btn btn-primary pull-right" role="button">
                                Create new...
                            </a>
                        @endif

                        @if (Auth::user()->identity == "student")
                            <a href="{{ route('courses.apply') }}" class="btn btn-primary" role="button">
                                Apply for...
                            </a>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
