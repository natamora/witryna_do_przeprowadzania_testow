@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Create question</div>
                    </div>

                    <div class="panel-body">
                        @if (Auth::user()->identity == "teacher")
                            Choose question type <br/><br/>

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form class="form-inline" method="post" action="{{ route('courses.tests.questions.choose', [$course, $test]) }}">
                                {{ csrf_field() }}
                                <br>
                                <select class="form-control" name="question_type"> <!--Supplement an id here instead of using 'text'-->
                                    <option value="single-answer" selected >Single-answer question</option>
                                    <option value="multiple-answer">Multiple-answer question</option>
                                    <option value="open">Open question</option>
                                    <option value="categorization">Categorization question</option>
                                </select>
                                <div class="form-group">

                                    <button type="submit" class="btn btn-primary">
                                        Create question
                                    </button>

                                </div>
                            </form>


                            <br>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
