@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Edit test</div>
                    </div>


                    <div class="panel-body">
                        @if (Auth::user()->identity == "teacher")

                            <form class="form-horizontal" method="post" action="{{route('courses.tests.update',[$course,$test])}}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">
                                <div class="form-group">
                                    <label for="name" class="col-md-4 control-label">Test name: </label>
                                    <div class="col-md-6">
                                        <textarea id='name' class="form-control" type="text" name="name">{{$test['name']}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-md-4 control-label">Description: </label>
                                    <div class="col-md-6">
                                        <textarea id='description' class="form-control" type="text" name="description">{{$test['description']}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Save test
                                        </button>
                                    </div>
                                </div>

                            </form>
                            <form class="form-horizontal" method="post" action="{{route('courses.tests.destroy',[$course,$test])}}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Delete test
                                        </button>
                                    </div>
                                </div>

                            </form>
                            @if($test->questions()->exists())Click to edit question:
                            @endif

                            @foreach($test->questions as $question)

                                @switch($question['question_type'])
                                    @case('single-answer')
                                    @case('multiple-answer')

                                        <div class="list-group">
                                            <a href="{{route('courses.tests.questions.edit',[$course,$test,$question])}}" class="list-group-item list-group-item-action flex-column align-items-start">
                                                <div class="d-flex w-100 justify-content-between">
                                                    <h5 class="mb-1"><b>{{ $question['content'] }}</b></h5>
                                                    <p align="right" class="text-primary">{{$question['question_type']}}</p>
                                                </div>

                                                @foreach($question->all_answers as $answer)
                                                    @if(DB::table('correct_answers')->whereAnswerId($answer['id'])->whereQuestionId($question['id'])->count()>0)
                                                        <p class="text-success ">{{$answer['answer']}}</p>
                                                    @else
                                                        <p class="text-primary">{{$answer['answer']}}</p>
                                                    @endif
                                                @endforeach

                                            </a>
                                        </div>
                                    @break

                                    @case('categorization')

                                        <div class="list-group">
                                            <a href="{{route('courses.tests.questions.edit',[$course,$test,$question])}}" class="list-group-item list-group-item-action flex-column align-items-start">
                                                <div class="d-flex w-100 justify-content-between">
                                                    <h5 class="mb-1"><b>{{ $question['content'] }}</b></h5>
                                                    <p align="right" class="text-primary">{{$question['question_type']}}</p>
                                                </div>

                                                    @foreach($question->categorization_answers as $answer)
                                                        <p class="text-primary"> <b>{{$answer['first']}}</b> {{$answer['second']}}</p>
                                                    @endforeach
                                            </a>
                                        </div>
                                    @break

                                    @case('open')

                                        <div class="list-group">
                                            <a href="{{route('courses.tests.questions.edit',[$course,$test,$question])}}" class="list-group-item list-group-item-action flex-column align-items-start">

                                                <div class="d-flex w-100 justify-content-between">
                                                    <h5 class="mb-1"><b>{{ $question['content'] }}</b></h5>
                                                    <p align="right" class="text-primary">{{$question['question_type']}}</p>
                                                </div>
                                            </a>
                                        </div>
                                    @break
                                @endswitch
                            @endforeach


                            <a href="{{route('courses.tests.questions.index',[$course,$test])}}" class="btn btn-primary pull-right" role="button"> Create question </a>

                        @endif
                            <a href="{{route('courses.tests.show',[$course,$test])}}"> Come back to test details... </a>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection