@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Tests</div>
                    </div>
                    <div class="panel-body">

                        {{--for teacher--}}
                        @if (Auth::user()->identity == "teacher")
                            Page to show statistics of a test<br>
                            <?php
                                $today = new DateTime("now",new DateTimeZone('Europe/Warsaw'));

                            ?>
                        <strong class="text-primary">Test name: </strong> {{ $test->name }}<br>
                        <strong class="text-primary">Actual release:</strong>
                            @if($test->release_date==null)
                                not set
                            @else
                                {{$test->release_date}}
                            @endif
                        <br>
                        <strong class="text-primary">Actual deadline:</strong>
                            @if($test->deadline==null)
                                not set
                            @else
                                {{$test->deadline}}
                            @endif
                        </br>
                        </br>


                                <form class="form-horizontal" method="post" action="{{route('courses.tests.update',[$course,$test])}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="PUT">
                                    <div class="form-group">
                                        <label for="release_date" class="col-md-7 control-label">Release date: </label>
                                        <div class="col-md-4">
                                            <input id='release_date' class="form-control" type="datetime-local" name="release_date">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="deadline" class="col-md-7 control-label">Deadline: </label>
                                        <div class="col-md-4">
                                            <input id='deadline' class="form-control" type="datetime-local" name="deadline">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-9">
                                            <button type="submit" class="btn btn-primary">
                                            Share test
                                            </button>
                                        </div>
                                    </div>
                                </form>



                            <?php
                            $today = new DateTime("now",new DateTimeZone('Europe/Warsaw'));
                            $deadline = new DateTime($test['deadline'],new DateTimeZone('Europe/Warsaw'));
                            ?>
                            @if ($deadline < $today)
                                Download Raport
                                <a href="{{ route('courses.tests.reports.download',[$course,$test]) }}">Download raport...</a>
                                </br>
                            @endif


                        @endif

                        @if (Auth::user()->identity == "student")
                            <?php
                            $today = new DateTime("now",new DateTimeZone('Europe/Warsaw'));
                            $deadline = new DateTime($test['deadline'],new DateTimeZone('Europe/Warsaw'));
                            $release = new DateTime($test['release_date'],new DateTimeZone('Europe/Warsaw'));
                            ?>

                            @if ($deadline > $today and $release < $today)
                                @if (DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->value('finished') == false)
                                    You can only attempt this test once. <br>
                                    Are you sure you want to do this right now? <br>
                                    <a href="{{route('courses.tests.solve', [$course,$test,1])}}">Solve Now!</a>
                                @else
                                    Already solved.
                                @endif
                            @endif
                        @endif

                        @if (Auth::user()->identity == "teacher")
                            </br></br>
                            <h5><b>Checking answers for open questions:</b></h5>
                            @foreach($test->questions as $question)
                                @if($question['question_type'] == 'open')
                                    <ul class="list-group">
                                        @foreach($question->open_questions as $answer)
                                            @if(isset($answer['is_correct']) == false)
                                                <strong>{{ $question['content'] }}</strong>
                                                <li class="list-group-item"> {{$answer['long_answer']}}
                                                <form class="form-inline" method="post" action="{{ route('courses.tests.checkOpenQuestion', [$course, $test]) }}">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" value="{{$answer->id}}" name="answer_id">
                                                    <input type="hidden" value="{{$question->id}}" name="question_id">
                                                    <select class="form-control" name="is_correct">
                                                        <option value="true">Correct</option>
                                                        <option value="false">Incorrect</option>
                                                    </select>
                                                    <button type="submit" class="btn btn-primary align-right">
                                                        Press
                                                    </button>
                                                </form>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                    @endif
                            @endforeach
                            <h5><b>Showing question statistics:</b></h5>
                            @foreach($test->questions as $question)
                                    <h4 class="pull-right text-primary">{{ $question['content'] }} </h4>

                                    <u class="text-primary"><i>Statystyki:</i></u> <br>
                                    <i >
                                        Wszystkich odpowiedzi:
                                        {{DB::table('question_stats')->where('question_id', $question->id)->value('correct') +
                                        DB::table('question_stats')->where('question_id', $question->id)->value('incorrect')}}
                                        <br>
                                        Poprawnych: {{DB::table('question_stats')->where('question_id', $question->id)->value('correct')}} <br>
                                        Błędnych: {{DB::table('question_stats')->where('question_id', $question->id)->value('incorrect')}}
                                    </i>
                                        <br>
                                        <br>
                                @if($question['question_type'] == 'open')

                                    @foreach($question->open_questions as $answer)
                                        <h4>Odpowiedzi: </h4>
                                        @if(isset($answer['is_correct']))
                                            <li class="list-group-item"> {{$answer['long_answer']}}</li>
                                        @endif
                                    @endforeach

                                    <h5>Poprawne to:</h5>
                                    <ul class="list-group">
                                        @foreach($question->open_questions as $poprawne)
                                            @if(isset($poprawne['is_correct']))
                                                @if($poprawne['is_correct'] == 1)
                                                    <li class="list-group-item"> {{$poprawne['long_answer']}}</li>
                                                @endif
                                            @endif
                                        @endforeach
                                    </ul>

                                    <br>


                                    @endif
                            @endforeach
                            </br>
                            <br>
                            <a href="{{ route('courses.tests.edit', [$course,$test]) }}" class="btn btn-primary pull-right ">Edit test...</a>

                            <a href="{{route('courses.show',[$course,$test])}}"> Come back to course </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection