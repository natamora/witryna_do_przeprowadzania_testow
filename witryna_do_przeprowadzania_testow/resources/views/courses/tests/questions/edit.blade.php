@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Edit question</div>
                    </div>
                    <div class="panel-body">

                        @if (Auth::user()->identity == "teacher")

                            <form class="form-horizontal" method="post" action="{{route('courses.tests.questions.update',[$course,$test,$question])}}">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">
                                <div class="form-group">
                                    <label for="question" class="col-md-4 control-label">Question: </label>
                                    <div class="col-md-6">
                                        <textarea id='question' class="form-control" type="text" name="question">{{$question['content']}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Save question
                                        </button>
                                    </div>
                                </div>

                            </form>
                            <form class="form-horizontal" method="post" action="{{route('courses.tests.questions.destroy',[$course,$test,$question])}}">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Delete
                                        </button>
                                    </div>
                                </div>

                            </form>

                            @switch($question['question_type'])
                                @case('single-answer')
                                @case('multiple-answer')

                                @if($question->all_answers()->exists())Click to edit answer:
                                @endif
                                    @foreach($question->all_answers as $answer)
                                        <div class="list-group ">
                                            <a href="{{route('courses.tests.questions.answers.edit',[$course,$test,$question,$answer->id])}}" class="list-group-item list-group-item-action flex-column align-items-start ">

                                                @if(DB::table('correct_answers')->whereAnswerId($answer['id'])->whereQuestionId($question['id'])->count()>0)
                                                    <p class="text-success ">{{$answer['answer']}}</p>
                                                @else
                                                    <p class="text-primary">{{$answer['answer']}}</p>
                                                @endif

                                            </a>


                                        </div>

                                    @endforeach
                                    <a href="{{route('courses.tests.questions.answers.create',[$course,$test,$question])}}" class="btn btn-primary pull-right" role="button"> Add answer</a>
                                @break

                                @case('categorization')
                                    @if($question->categorization_answers()->exists())Click to edit answer:
                                    @endif

                                    @foreach($question->categorization_answers as $answer)
                                        <div class="list-group">
                                            <a href="{{route('courses.tests.questions.answers.edit',[$course,$test,$question,$answer->id])}}" class="list-group-item list-group-item-action flex-column align-items-start">
                                                    <p class="text-primary"> <b>{{$answer['first']}}</b> {{$answer['second']}}</p>
                                            </a>
                                        </div>
                                    @endforeach
                                    <a href="{{route('courses.tests.questions.answers.create',[$course,$test,$question])}}" class="btn btn-primary pull-right" role="button"> Add answer</a>
                                @break

                            @endswitch

                            <a href="{{route('courses.tests.edit',[$course,$test])}}">Back to edit test...</a>
                        @else
                           {{view('/errors/404')}}
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection