<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('register a new user');

$I->amOnPage('/');
$I->seeLink('Register');
$I->click('Register');
$I->seeCurrentUrlEquals('/register');


$I->click('button[type=submit]');
$I->seeCurrentUrlEquals('/register');
$I->seeElement('strong');


$name = 'Jane Doe';
$email = 'jane@doe.com';
$password = 'jane123';
$wrong_password = 'Jane123';


$I->dontSeeInDatabase('users', ['email' => $email]);

$I->see('Student', 'select.form-control');
$I->selectOption('Identity', 'Teacher');
$I->see('Teacher', 'select.form-control');

$I->fillField('name', $name);
$I->fillField('email', $email);
$I->fillField('password', $password);
$I->fillField('password_confirmation', $wrong_password);
$I->click('button[type=submit]');


$I->seeCurrentUrlEquals('/register');
$I->see('The password confirmation does not match.');


$I->fillField('password', $password);
$I->fillField('password_confirmation', $password);
$I->click('button[type=submit]');

$I->seeCurrentUrlEquals('/home');
$I->seeInDatabase('users', ['email' => $email]);

$I->dontSeeLink('Login');
$I->dontSeeLink('Register');

$I->see($name, 'a.dropdown-toggle');
$I->see('You are logged in!', 'div.panel > div.panel-body');