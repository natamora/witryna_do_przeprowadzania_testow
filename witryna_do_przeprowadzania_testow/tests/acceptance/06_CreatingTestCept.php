<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('create test in existing course');

$teacher_name = 'Rasmus Lerdorf';
$teacher_email = 'teacher@mail.com';
$teacher_password = 'rsms123';

$course = 'PHP II';
$description = 'advanced programming with PHP';

$I->haveInDatabase('users', [
    'email' => $teacher_email,
    'name' => $teacher_name,
    'password' => password_hash($teacher_password, PASSWORD_DEFAULT),
    'identity' => 'teacher',
]);

$teacher_id = $I->grabFromDatabase('users', 'id', array('email' => $teacher_email));

$I->haveInDatabase('courses', [
    'name' => $course,
    'description' => $description,
    'teacher_id' => $teacher_id,
]);

$course_id = $I->grabFromDatabase('courses', 'id', array('name'=> $course));


$I->amOnPage('/login');
$I->fillField('email', $teacher_email);
$I->fillField('password', $teacher_password);
$I->click('button[type=submit]');

$I->amOnPage('/courses/' . $course_id);

$I->seeLink('Manage tests');
$I->click('Manage tests');

$I->seeInCurrentUrl('/courses/' . $course_id . '/tests');
$I->see('Tests','div.panel-heading');
$I->seeLink('Create new...');
$I->click('Create new...');
$I->seeInCurrentUrl('/courses/' . $course_id . '/tests/create');

$test_name = 'test 1';
$test_description = 'some test description';

$I->dontSeeInDatabase('tests', [
    'name' => $test_name,
    'description' => $test_description,
    'course_id' => $course_id,
]);

$I->fillField('name', $test_name);
$I->fillField('description', $test_description);

$I->click('Create');

$I->seeInDatabase('tests', [
    'name' => $test_name,
    'description' => $test_description,
    'course_id' => $course_id,
]);

$test_id = $I->grabFromDatabase('tests', 'id', array('name'=> $test_name));
$I->seeInCurrentUrl('/courses/' . $course_id . '/tests/' . $test_id . '/edit');

$I->seeLink('Come back to test details...');
$I->seeLink('Create question');
