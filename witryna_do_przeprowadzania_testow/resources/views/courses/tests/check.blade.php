@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Tests</div>
                    </div>
                    <div class="panel-body">
                        @if (Auth::user()->identity == "student")
                            @if (DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->value('finished') == false)
                                @if (DB::table('user_question')->where('user_id', Auth::id())->where('question_id', $question->id)->value('answered') == false)
                                    @if($question->question_type == 'single-answer')
                                        @if ($good == 1)
                                            Correct answer!
                                        @else
                                            Wrong answer. <br>
                                            Correct should be: <br> {{$text}} <br>
                                        @endif
                                            <?php DB::table('user_question')->where('user_id', Auth::id())->where('question_id', $question->id)->update(array('answered' => true));?>
                                            <a href="{{route('courses.tests.solve', [$course,$test,$next])}}" class="btn btn-primary ">Next!</a>
                                    @elseif($question->question_type == 'open')
                                        This question needs to be verified by teacher. <br>
                                        <?php DB::table('user_question')->where('user_id', Auth::id())->where('question_id', $question->id)->update(array('answered' => true));?>
                                        <a href="{{route('courses.tests.solve', [$course,$test,$next])}}" class="btn btn-primary" >Next!</a>
                                    @elseif($question->question_type == 'multiple-answer')
                                        <br>
                                            @if ($good == 1)
                                                Correct answer!
                                            @else
                                                Wrong answer. <br>
                                                Correct should be: <br>
                                                @foreach($answers as $answer)
                                                    {{$answer['answer']}} <br>
                                                @endforeach
                                            @endif
                                            <?php DB::table('user_question')->where('user_id', Auth::id())->where('question_id', $question->id)->update(array('answered' => true));?>
                                        <a href="{{route('courses.tests.solve', [$course,$test,$next])}}" class="btn btn-primary" >Next!</a>
                                    @elseif($question->question_type == 'categorization')
                                        @if ($good == 0)
                                            Wrong answer. <br>
                                            Correct should be: <br>
                                            @for($i=0; $i < count($answer_array_first); $i++)
                                                {{$answer_array_first[$i]}} {{$answer_array_second[$i]}} <br>
                                            @endfor
                                        @elseif ($good == 1)
                                            Correct answer! <br>
                                        @endif
                                            <?php DB::table('user_question')->where('user_id', Auth::id())->where('question_id', $question->id)->update(array('answered' => true));?>
                                            <a href="{{route('courses.tests.solve', [$course,$test,$next])}}" class="btn btn-primary ">Next!</a>
                                    @endif
                                @else
                                    Sorry, this question was already answered. <br>
                                    <a href="{{route('courses.tests.solve', [$course,$test,$next])}}" class="btn btn-primary ">Next!</a>
                                @endif
                            @else
                                Sorry, this test is already solved.
                            @endif
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection