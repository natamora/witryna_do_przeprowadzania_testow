@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Create question</div>
                    </div>

                    <div class="panel-body">
                        @if (Auth::user()->identity == "teacher")

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {{--@switch($question_type)--}}

                                {{--@case("single-answer")          --}}{{--,$question_type--}}
                                    <form class="form-horizontal" method="post" action="{{ route('courses.tests.questions.getstore', [$course,$test,$choice]) }}">
                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label for="question" class="col-md-4 control-label">Question: </label>
                                            <div class="col-md-6">
                                                <textarea id='question' class="form-control" type="text" name="question"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    Create and add answers
                                                </button>
                                            </div>
                                        </div>

                                    </form>
                            <a href="{{route('courses.tests.edit',[$course,$test])}}"> Wróć do edit test </a>
                                {{--@break--}}

                                {{--@case("multiple_answer")--}}
                                    {{--<form method="post" action="{{ route('courses.tests.questions.store', [$course,$test,$question_type]) }}">--}}
                                        {{--{{ csrf_field() }}--}}
                                        {{--Name: <input type="text" name="name">--}}
                                        {{--<br>--}}
                                        {{--Description: <input type="text" name="description">--}}
                                        {{--<br>--}}
                                        {{--<input type="submit" value="Create">--}}
                                    {{--</form>--}}
                                {{--@break--}}

                                {{--@case("open")--}}
                                {{--<span> open </span>--}}
                                {{--@break--}}

                                {{--@default--}}
                                {{--<span>Something went wrong</span>--}}

                            {{--@endswitch--}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
