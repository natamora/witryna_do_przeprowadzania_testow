<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('login and logout a user');

$I->amOnPage('/');
$I->seeLink('Login');
$I->click('Login');
$I->seeCurrentUrlEquals('/login');

$name = 'Jane Doe';
$email = 'jane@doe.com';
$password = 'jane123';

$I->dontSeeInDatabase('users', [
    'email' => $email
]);

$I->fillField('email', $email);
$I->fillField('password', $password);
$I->click('button[type=submit]');

$I->seeCurrentUrlEquals('/login');
$I->see('These credentials do not match our records.');


$I->haveInDatabase('users', [
    'email' => $email,
    'name' => $name,
    'password' => password_hash($password, PASSWORD_DEFAULT),
    'identity' => 'teacher'
]);

$I->fillField('email', $email);
$I->fillField('password', $password);
$I->click('button[type=submit]');

$I->seeCurrentUrlEquals('/home');
$I->dontSeeLink('Login');
$I->dontSeeLink('Register');

$I->see($name, 'a.dropdown-toggle');
$I->see('You are logged in!', 'div.panel > div.panel-body');


$I->click($name);
$I->seeLink('Logout');
$I->seeLink('About');
$I->seeLink('Courses');

$I->click('Logout');
$I->seeCurrentUrlEquals('/logout');
$I->dontSee($name);