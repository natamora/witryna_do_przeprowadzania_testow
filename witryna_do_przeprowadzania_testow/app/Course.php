<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    function user()
    {
        return $this->belongsTo('App\User', 'teacher_id');
    }

    function users()
    {
        return $this->belongsToMany('App\User')->withPivot('confirmed')->withTimestamps();
    }
    function tests()
    {
        return $this->hasMany(Test::class);
    }
}
