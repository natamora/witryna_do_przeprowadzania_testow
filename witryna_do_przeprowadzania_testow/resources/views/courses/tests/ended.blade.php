@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Tests</div>

                    <div class="panel-body">
                        @if (Auth::user()->identity == "student")
                            Thanks for taking the test
                            Your results:
                            {{DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->value('points')}}
                            / {{$test->questions()->count()}} <br>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection