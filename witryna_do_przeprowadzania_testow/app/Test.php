<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    function course()
    {
        return $this->belongsTo('App\Course', 'course_id');
    }
    function questions()
    {
        return $this->hasMany(Question::class);
    }
    function random_questions()
    {
        return $this->hasMany(Random_question::class);
    }
    function users()
    {
        return $this->belongsToMany( 'App\User','user_points','test_id','user_id')
            ->withPivot('points')->withTimestamps();
    }
}
