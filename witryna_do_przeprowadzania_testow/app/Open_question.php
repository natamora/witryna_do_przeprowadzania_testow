<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class open_question extends Model
{
    function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    function question()
    {
        return $this->belongsTo('App\Question', 'question_id');
    }
}
