<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('create a course as a teacher');

$name = 'Jane Doe';
$email = 'jane@doe.com';
$password = 'jane123';
$course = 'PHP II';
$description = 'advanced programming with PHP';

$I->haveInDatabase('users', [
    'email' => $email,
    'name' => $name,
    'password' => password_hash($password, PASSWORD_DEFAULT),
    'identity' => 'teacher'
]);


$I->amOnPage('/courses');
$I->dontSee('Courses', 'div.panel-heading');
$I->seeCurrentUrlEquals('/login');

$I->fillField('email', $email);
$I->fillField('password', $password);
$I->click('button[type=submit]');


$I->seeCurrentUrlEquals('/courses');


$I->See('Courses', 'div.panel-heading');
$I->seeLink('Create new...');

$I->click('Create new...');
$I->seeCurrentUrlEquals('/courses/create');

$I->click('Create');
$I->see('The name field is required.');
$I->see('The description field is required.');

$I->dontSeeInDatabase('courses', [
    'name' => $course,
    'description' => $description,
]);


$I->fillField('name', $course);
$I->fillField('description', $description);
$I->click('Create');

$I->seeInDatabase('courses', [
    'name' => $course,
    'description' => $description,
]);

$id = $I->grabFromDatabase('courses', 'id', array('name'=> $course));

$I->seeCurrentUrlEquals('/courses');
$I->see($course, 'h5');
$I->seeLink($course);

$I->click($course);
$I->seeCurrentUrlEquals('/courses/' . $id);
$I->see($course);
$I->see($email);
$I->see($description);