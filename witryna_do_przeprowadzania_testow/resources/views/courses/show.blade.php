@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Details</div>
                    </div>
                    <div class="panel-body">
                        <strong>{{ $course->name }}</strong> <br>
                        <strong>Teacher: </strong>{{ $course->user->email }} <br>
                        <strong>Description: </strong>{{ $course->description }} <br>

                        @if (Auth::user()->identity == "teacher")
                            <br> <h5><b>Students:</b> </h5>
                            <ul class="list-group">
                                @foreach($course->users as $student)
                                    @if($student->pivot->confirmed == true)
                                        <a href="{{route('courses.students', [$course, $student])}}" class="list-group-item list-group-item-action flex-column align-items-start">
                                                <b>{{ $student->name }}</b>
                                        </a>
                                    @endif
                                @endforeach
                            </ul>
                            <h5><b>Not confirmed students: </b></h5>

                                @foreach($course->users as $student)
                                    @if($student->pivot->confirmed == false)

                                        <form  class="form-inline" method="post" action="{{ route('courses.confirm', $course) }}">
                                            {{ csrf_field() }}
                                            <input type="hidden" value="{{$student->id}}" name="user_id">

                                            <label class="mr-sm-2 text-primary" for="confirmation">{{ $student->name }}</label>
                                            <select id="confirmation" class="form-control" name="confirmation">
                                                <option value="true">Confirm</option>
                                                <option value="false">Reject</option>
                                            </select>
                                            <button type="submit" class="btn btn-primary align-right">
                                                Press
                                            </button>

                                        </form>
                                    @endif
                                @endforeach

                            <a href="{{ route('courses.tests.index', $course) }}" class="btn btn-primary pull-right" >Manage tests</a>
                        @endif

                        @if (Auth::user()->identity == "student")
                            @if(Auth::user()->courses->where('id', $course->id)->first()->pivot->confirmed  == false)
                                You are not confirmed
                            @else
                                <strong>Available tests: </strong><br>
                                @foreach($course->tests as $test)

                                    <?php
                                    $today = new DateTime("now",new DateTimeZone('Europe/Warsaw'));

                                    $deadline = new DateTime($test['deadline'],new DateTimeZone('Europe/Warsaw'));
                                    $release = new DateTime($test['release_date'],new DateTimeZone('Europe/Warsaw'));
                                    ?>

                                    @if ($deadline > $today and $release < new $today)
                                    <li>
                                        <strong>{{ $test['name'] }}</strong>
                                        @if(DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->value('finished') == true)
                                            Points: {{DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->value('points')}}
                                            / {{$test->questions()->count()}}
                                        @else
                                            <a href="{{ route('courses.tests.show', [$course,$test]) }}">Solve</a>
                                        @endif
                                    </li>
                                    @endif

                                @endforeach
                            @endif
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
