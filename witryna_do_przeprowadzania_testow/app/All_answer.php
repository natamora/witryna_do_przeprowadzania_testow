<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class All_answer extends Model
{
    function questions()
    {
        return $this->belongsToMany( 'App\Question','correct_answers','answer_id','question_id')->withTimestamps();
    }
}
