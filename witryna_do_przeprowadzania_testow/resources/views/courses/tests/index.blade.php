@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Tests</div>
                    </div>

                    <div class="panel-body">

                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                            @if (Auth::user()->identity == "student")
                                    @foreach($course->tests as $test)
                                        <?php
                                        $today = new DateTime("now",new DateTimeZone('Europe/Warsaw'));
                                        $deadline = new DateTime($test['deadline']);
                                        $release = new DateTime($test['release_date']);
                                        ?>

                                        @if ($deadline > $today and $release < $today)
                                        <li>
                                            <strong>{{ $test['name'] }}</strong>
                                            <a href="{{ route('courses.tests.show', [$course,$test]) }}">details</a>
                                        </li>
                                        @endif
                                    @endforeach
                            @endif



                        @if (Auth::user()->identity == "teacher")
                            <div class="list-group">
                            @foreach($course->tests as $test)
                                <a href="{{ route('courses.tests.show', [$course,$test]) }}" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <strong>{{ $test['name'] }}</strong>
                                </a>
                            @endforeach
                            </div>



                            <a href="{{route('courses.index',$course )}}">Come back to course details...</a>
                            <a href="{{ route('courses.tests.create', $course) }}" class="btn btn-primary pull-right" role="button">
                                Create new...
                            </a>





                        @endif


                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection