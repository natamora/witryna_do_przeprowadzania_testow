<?php 
$I = new AcceptanceTester($scenario);
$I->wantTo('display homepage');

$I->amOnPage('/');

$I->seeInTitle('Witryna do przeprowadzania testów');
$I->see('Testing Site', 'div.title');