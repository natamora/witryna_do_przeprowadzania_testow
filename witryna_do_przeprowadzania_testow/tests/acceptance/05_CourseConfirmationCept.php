<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('confirm student to course');

$student_name = 'Jane Doe';
$student_email = 'jane@doe.com';
$student_password = 'jane123';

$teacher_name = 'Rasmus Lerdorf';
$teacher_email = 'teacher@mail.com';
$teacher_password = 'rsms123';

$course = 'PHP II';
$description = 'advanced programming with PHP';

$I->haveInDatabase('users', [
    'email' => $student_email,
    'name' => $student_name,
    'password' => password_hash($student_password, PASSWORD_DEFAULT),
    'identity' => 'student',
]);

$I->haveInDatabase('users', [
    'email' => $teacher_email,
    'name' => $teacher_name,
    'password' => password_hash($teacher_password, PASSWORD_DEFAULT),
    'identity' => 'teacher',
]);

$student_id = $I->grabFromDatabase('users', 'id', array('email' => $student_email));
$teacher_id = $I->grabFromDatabase('users', 'id', array('email' => $teacher_email));

$I->haveInDatabase('courses', [
    'name' => $course,
    'description' => $description,
    'teacher_id' => $teacher_id,
]);

$course_id = $I->grabFromDatabase('courses', 'id', array('name'=> $course));

$I->haveInDatabase('course_user',[
    'user_id' => $student_id,
    'course_id' => $course_id,
    'confirmed' => false
]);


$I->amOnPage('/login');
$I->fillField('email', $teacher_email);
$I->fillField('password', $teacher_password);
$I->click('button[type=submit]');

$I->click($teacher_name);
$I->click('Courses');
$I->see($course);
$I->click($course);

$I->seeInCurrentUrl('/courses/' . $course_id);
$I->see('Not confirmed students:', 'h5');
$I->see($student_name, 'label');

$I->selectOption('confirmation', 'Confirm');
$I->click('Press');


$I->SeeInDatabase('course_user',[
    'user_id' => $student_id,
    'course_id' => $course_id,
    'confirmed' => true
]);