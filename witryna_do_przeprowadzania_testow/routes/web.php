<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/about', function () {
    return view('about');
});

Route::group(['middleware' => 'auth'], function() {

    Route::post('/courses/apply', 'CoursesController@register')->name('courses.register');
    Route::post('/courses/{course}', 'CoursesController@confirm')->name('courses.confirm');
    Route::get('/courses/apply', 'CoursesController@apply')->name('courses.apply');
    //Route::post('/courses/{course}/tests/{test}/questions/create', 'Courses\Tests\QuestionsController@create')->name('courses.tests.questions.create');

    Route::get('/courses/{course}/tests/{test}/show', 'Courses\Tests\ReportsController@download')->name('courses.tests.reports.download');
    Route::post('/courses/{course}/tests/{test}/show', 'Courses\TestsController@checkOpenQuestion')
        ->name('courses.tests.checkOpenQuestion');
    Route::get('/courses/{course}/tests/{test}/solve/{next}', 'Courses\TestsController@solve')->name('courses.tests.solve');
    Route::post('/courses/{course}/tests/{test}/check/{next}', 'Courses\TestsController@check')->name('courses.tests.check');

    Route::post('/courses/{course}/tests/{test}/questions/choose', 'Courses\Tests\QuestionsController@choose')->name('courses.tests.questions.choose');
    Route::post('/courses/{course}/tests/{test}/questions/getstore/{choice}', 'Courses\Tests\QuestionsController@getstore')->name('courses.tests.questions.getstore');

    Route::post('/courses/{course}/students/{student}', 'Courses\StudentController@show')->name('courses.students');

    Route::resource('/courses.tests.questions.answers','Courses\Tests\Questions\AnswersController');
    Route::resource('/courses.tests.questions', 'Courses\Tests\QuestionsController');
    Route::resource('/courses.students', 'Courses\StudentController');
    Route::resource('/courses.tests', 'Courses\TestsController');
    Route::resource('/courses', 'CoursesController');

});

