@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Tests</div>
                    </div>

                    <div class="panel-body">
                        </br>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (Auth::user()->identity == "teacher")
                            <form class="form-horizontal" method="post" action="{{ route('courses.tests.store', $course) }}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name" class="col-md-4 control-label">Test Name:</label>
                                    <div class="col-md-6">
                                        <input id="name" class="form-control" type="text" name="name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-md-4 control-label">Description:</label>
                                    <div class="col-md-6">
                                    <input id="description" type="text" class="form-control" name="description">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="release_date" class="col-md-4 control-label">Release date:</label>
                                    <div class="col-md-6">
                                        <input id="release_date" type="datetime-local" class="form-control" name="release_date">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="deadline" class="col-md-4 control-label">Deadline:</label>
                                    <div class="col-md-6">
                                        <input id="deadline" type="datetime-local" class="form-control" name="deadline">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Create
                                        </button>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
