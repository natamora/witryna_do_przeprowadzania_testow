@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Edit answer</div>
                    </div>
                    <div class="panel-body">

                        @if (Auth::user()->identity == "teacher")

                            @switch($question['question_type'])
                                @case('single-answer')
                                @case('multiple-answer')
                                    <form class="form-horizontal" method="post" action="{{route('courses.tests.questions.answers.update',[$course,$test,$question,$answerid])}}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="PUT">
                                        <div class="form-group">
                                            <label for="answer" class="col-md-4 control-label">Answer: </label>
                                            <div class="col-md-6">
                                                <textarea id='answer' class="form-control" type="text" name="answer">{{DB::table('all_answers')->where('id',$answerid)->value('answer')}}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    Save answer
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                        <form class="form-horizontal" method="post" action="{{route('courses.tests.questions.answers.destroy',[$course,$test,$question,$answerid])}}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <div class="form-group">
                                                <div class="col-md-8 col-md-offset-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </form>


                                @break
                                @case('categorization')
                                    <form class="form-horizontal" method="post" action="{{route('courses.tests.questions.answers.update',[$course,$test,$question,$answerid])}}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="PUT">
                                        <div class="form-group">
                                            <label for="first" class="col-md-4 control-label">First: </label>
                                            <div class="col-md-6">
                                                <textarea id='first' class="form-control" type="text" name="first">{{DB::table('categorization_answers')->where('id',$answerid)->value('first')}}</textarea>
                                            </div>
                                            <label for="second" class="col-md-4 control-label">Second: </label>
                                            <div class="col-md-6">
                                                <textarea id='second' class="form-control" type="text" name="second">{{DB::table('categorization_answers')->where('id',$answerid)->value('second')}}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-4">
                                                <input type="submit" value="Save answer">
                                            </div>
                                        </div>

                                    </form>

                                    <form class="form-horizontal" method="post" action="{{route('courses.tests.questions.answers.destroy',[$course,$test,$question,$answerid])}}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}

                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-4">
                                                <input type="submit" value="Delete">
                                            </div>
                                        </div>
                                    </form>

                                @break
                            @endswitch

                        @else
                            {{view('/errors/404')}}
                        @endif


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection