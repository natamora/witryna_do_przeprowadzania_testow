<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Report</title>
</head>
<body>
<div>
        <div class="container">
            <table class="table">
                <tr>
                    <th>Test</th>
                    <th scope="col">{{strtoupper($test->name)}}</th>
                </tr>
                <tr>
                    <th>Course</th>
                    <th scope="col">{{$course->id}}{{$course->name}}</th>
                </tr>
                <tr>
                    <th>Description</th>
                    <th scope="col">{{$course->description}}</th>
                </tr>
                <tr>
                    <th>Teacher</th>
                    <th scope="col">{{$course->user->name}}</th>
                </tr>
                <tr>
                    <th>Teacher email</th>
                    <th scope="col">{{$course->user->email}}</th>
                </tr>
                <tr>
                    <th>Date</th>
                    <th scope="col">{{$test->release_date}} - {{$test->deadline}}</th>
                </tr>
            </table>
        </div>
        <div>
        <?php
        $count_questions = $test->questions->count()
        ?>
        <br>
        <h3>Students</h3>
        <table class="table table-striped">
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Correct</th>
                    <th scope="col">Incorrect</th>
                    <th scope="col">Max</th>
                    <th scope="col">In percents</th>
                </tr>
               @foreach($test->users as $user)
                <tr>
                    <th scope="row">{{$user->name }}</th>
                    <th>{{$val1 = DB::table('user_points')->where('user_id', $user->id)->where('test_id', $test->id)->value('points')}}</th>
                    <th>{{$count_questions-$val1}}</th>
                    <th>{{$count_questions}}</th>
                    <th>{{$val1/$count_questions*100}}%</th>
                </tr>
                @endforeach
        </table>
        <br>
        <h3>Questions</h3>
        <table class="table table-striped">
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Type</th>
                    <th scope="col">Correct</th>
                    <th scope="col">Incorrect</th>
                    <th scope="col">Max</th>
                    <th scope="col">In percents</th>
                </tr>
                @foreach($test->questions as $question)
                <tr>
                    <th scope="row">{{$question->id}}</th>
                    <th>{{$question->question_type}}</th>
                    <th>{{$val1 = DB::table('question_stats')->where('question_id', $question->id)->value('correct')}} </th>
                    <th>{{$val2 = DB::table('question_stats')->where('question_id', $question->id)->value('incorrect')}}</th>
                    <th>{{$val1+$val2}}</th>
                    <th>
                        @if($val1+$val2 != 0)
                            {{$val1/($val1+$val2)*100}}%
                        @else
                            0%
                        @endif
                    </th>
                </tr>
                @endforeach
        </table>
        </div>
</div>
</body>
</html>















