@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Tests</div>
                    </div>

                    <div class="panel-body">

                        @if (Auth::user()->identity == "student")
                            @if (DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->value('finished') == false)
                                Points: {{DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->value('points')}} / {{$test->questions()->count()}} <br>
                                Question {{$next}} / {{$test->questions()->count()}} <br>
                                <h3>{{$question['content']}} <br></h3>
                                @if ($question->question_type == 'single-answer')
                                    <form method="post" action="{{ route('courses.tests.check', [$course, $test, $next]) }}">
                                        {{ csrf_field() }}
                                        @foreach($answer_array as $answer)
                                            {{$answer['answer']}} <input type="radio" name="choice" value="{{$answer['answer']}}"> <br>
                                        @endforeach
                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    Next question
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                @elseif ($question->question_type == 'multiple-answer')
                                    <form method="post" action="{{ route('courses.tests.check', [$course, $test, $next]) }}">
                                        {{ csrf_field() }}
                                        @foreach($answer_array as $answer)
                                            {{$answer['answer']}} <input type="checkbox" name="check_list[]" value="{{$answer['answer']}}"> <br>
                                        @endforeach
                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    Next question
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                @elseif ($question->question_type == 'open')
                                    <form method="post" action="{{ route('courses.tests.check', [$course, $test, $next]) }}">
                                        {{ csrf_field() }}
                                        <textarea type="text" name="choice" style="width:400px; height:100px;" > </textarea><br>
                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-9">
                                                <button type="submit" class="btn btn-primary">
                                                    Next question
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                @elseif ($question->question_type == 'categorization')
                                    <p>Write answer in the field. You can choose from following:</p>
                                    <i class="col-md-8">(
                                        @foreach($answer_array_second as $answer)
                                            {{$answer}}
                                        @endforeach
                                            )</i>
                                    <br>
                                    <br>
                                    <form class="form-horizontal" method="post" action="{{ route('courses.tests.check', [$course, $test, $next]) }}">
                                        {{ csrf_field() }}
                                        @foreach($answer_array_first as $answer)
                                            <div class="form-group">
                                                <label for="{{$answer}}" class="col-md-4 control-label">{{$answer}}</label>
                                                <div class="col-md-6">
                                                    <input id="{{$answer}}" type="text" class="form-control" name="{{$answer}}" value="">
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    Next question
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                @endif
                            @else
                                Sorry, this test is already solved.
                            @endif

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection