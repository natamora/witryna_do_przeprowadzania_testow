@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Apply</div>
                    </div>
                    <div class="panel-body">
                        Page to apply for the course
                        <form class="form-horizontal" method="post" action="{{ route('courses.register') }}">
                            {{ csrf_field() }}
                            <br>
                                <select class="form-control" name="course">
                                    @foreach($courses as $course)
                                        <option value="{{$course->id}}">{{$course->id}} {{$course->name}} {{$course->user->email}}</option>
                                    @endforeach
                                </select>
                            <br>
                            <button type="submit" class="btn btn-primary">Apply</button>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
