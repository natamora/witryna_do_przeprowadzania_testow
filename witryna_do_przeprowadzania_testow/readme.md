#Testing Site

###Before you start
```bash
sudo rm /usr/bin/composer
wget https://getcomposer.org/download/1.5.5/composer.phar
sudo mv composer.phar /usr/bin/composer
sudo chmod +x /usr/bin/composer
```

###How to open existing project?
```bash
git clone https://bitbucket.org/kinia333x/php_2017_witryna_do_przeprowadzania_testow
cd php_2017_witryna_do_przeprowadzania_testow/witryna_do_przeprowadzania_testow
composer install --ignore-platform-reqs
cp .env.example .env
php artisan key:generate
php artisan migrate
php artisan serve
```

###How to execute tests?

```bash
#Serever must be running while executing tests!

# Generating database dump for Codeception tests:
mysqldump -u test test -p > tests/_data/dump.sql

# Running acceptance tests:
vendor/bin/codecept run acceptance
```

