<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question_stat extends Model
{
    function question(){
        return $this->belongsTo('App\Question', 'question_id');
    }
}
