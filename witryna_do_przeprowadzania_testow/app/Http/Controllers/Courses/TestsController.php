<?php

namespace App\Http\Controllers\Courses;
use App\Random_question;
use App\Course;
use App\Question;
use App\Test;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Open_question;

class TestsController extends Controller
{

    public function index(Course $course)
    {
        //check if this is proper student
        if(Auth::user()->identity == "student")
        {
            if(DB::table('course_user')->where('course_id', $course->id)->where('user_id', Auth::id())->value('user_id')  != Auth::id()){
                $courses = Course::all();
                return view('courses.index')->withCourses($courses);
            }
        }

        return view('courses.tests.index')->withCourse($course);
    }


    public function create(Course $course)
    {
        return view('courses.tests.create')->withCourse($course);
    }

    public function solve(Course $course, Test $test, $next)
    {
        if(DB::table('course_user')->where('course_id', $course->id)->where('user_id', Auth::id())->value('user_id')  != Auth::id()){
            $courses = Course::all();
            return view('courses.index')->withCourses($courses);
        }

        if ($next == 1)
        {
            for ($i = 0; $i < count($test->questions); $i++)
            {
                $random_indexes[] = $i;
            }

            shuffle($random_indexes);

            for ($i = 0; $i < count($test->questions); $i++)
            {
                $rand_question=new Random_question();
                $rand_question->question_id=$random_indexes[$i];
                $test->random_questions()->save($rand_question);
            }

            Test::find($test->id)->users()->attach(Auth::id(), ['points' => 0]);
        }

        if ($next > $test->questions()->count())
        {
            DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->update(array('finished' => true));
            return view('courses.tests.ended')->with('test', $test)->with('course', $course);
        }

        $question = $test->questions[$test->random_questions[$next-1]->question_id];
        $next_id = $question->id;

        if($question->question_type == 'categorization')
        {
            Question::find($question->id)->users()->attach(Auth::id(), ['answered' => false]);

            $answer_array_first = array();
            $answer_array_second = array();
            foreach ($question->categorization_answers as $answer)
            {
                $answer_array_first[] = $answer['first'];
                $answer_array_second[] = $answer['second'];
            }
            shuffle($answer_array_first);
            shuffle($answer_array_second);

            return view('courses.tests.solve')->withCourse($course)->with('test', $test)
                ->with('next', $next)->with('next_id', $next_id)->withQuestion($question)
                ->with('answer_array_first', $answer_array_first)
                ->with('answer_array_second', $answer_array_second);
        }
        else{
            Question::find($question->id)->users()->attach(Auth::id(), ['answered' => false]);

            $answer_array = array();
            foreach($question->all_answers as $answer)
                $answer_array[] = $answer;

            shuffle($answer_array);

            return view('courses.tests.solve')->withCourse($course)->withTest($test)
                ->with('next', $next)->with('next_id', $next_id)->withQuestion($question)
                ->with('answer_array', $answer_array);
        }

    }

    public function check(Request $request, Course $course, Test $test, $next)
    {
        if (DB::table('course_user')->where('course_id', $course->id)->where('user_id', Auth::id())->value('user_id')  != Auth::id()){
            $courses = Course::all();
            return view('courses.index')->withCourses($courses);
        }

        $questions = $test->questions;
        $question = $questions[$test->random_questions[$next-1]->question_id];


        if ($question->question_type == 'categorization')
        {
            if (DB::table('user_question')->where('user_id', Auth::id())->where('question_id', $question->id)->value('answered') == false) {

                $good = 1;

                foreach ($question->categorization_answers as $answer) {
                    $answer_second = $request->input($answer['first']);
                    if ($answer_second != $answer['second']) {
                        $good = 0;

                        $answer_array_first = array();
                        $answer_array_second = array();
                        foreach ($question->categorization_answers as $answer) {
                            $answer_array_first[] = $answer['first'];
                            $answer_array_second[] = $answer['second'];
                        }

                        $next++;

                        $incorrect = DB::table('question_stats')->where('question_id', $question->id)->value('incorrect');
                        $incorrect++;
                        DB::table('question_stats')->where('question_id', $question->id)->update(array('incorrect' => $incorrect));

                        return view('courses.tests.check')->withCourse($course)->withTest($test)
                            ->with('next', $next)->with('answer_array_first', $answer_array_first)
                            ->with('good', $good)->with('answer_array_second', $answer_array_second)
                            ->withQuestion($question)->with('answered', false);
                    }
                }

                $points = DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->value('points');
                $points++;

                DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->update(array('points' => $points));

                $next++;

                $correct = DB::table('question_stats')->where('question_id', $question->id)->value('correct');
                $correct++;
                DB::table('question_stats')->where('question_id', $question->id)->update(array('correct' => $correct));

                return view('courses.tests.check')->withCourse($course)->withTest($test)
                    ->with('next', $next)->with('good', $good)->withQuestion($question)
                    ->with('answered', false);
            }
            else {
                $next++;
                return view('courses.tests.check')->withCourse($course)->withTest($test)
                    ->withQuestion($question)->with('next', $next)->with('answered', true);
            }
        }
        else if ($question->question_type == 'single-answer')
        {
            if (DB::table('user_question')->where('user_id', Auth::id())->where('question_id', $question->id)->value('answered') == false) {

                $answers = $question->answers;
                $answer = $answers[0];

                if ($request->choice == $answer['answer']) {
                    $good = 1;
                    $points = DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->value('points');
                    $points++;
                    DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->update(array('points' => $points));

                    $correct = DB::table('question_stats')->where('question_id', $question->id)->value('correct');
                    $correct++;
                    DB::table('question_stats')->where('question_id', $question->id)->update(array('correct' => $correct));

                }
                else {
                    $good = 0;
                    $incorrect = DB::table('question_stats')->where('question_id', $question->id)->value('incorrect');
                    $incorrect++;
                    DB::table('question_stats')->where('question_id', $question->id)->update(array('incorrect' => $incorrect));

                }

                $next++;

                return view('courses.tests.check')->withCourse($course)->withTest($test)
                    ->with('next', $next)->with('text', $answer['answer'])->with('good', $good)
                    ->withQuestion($question)->with('answered', false);
            }
            else {

                $next++;

                return view('courses.tests.check')->withCourse($course)->withTest($test)
                    ->withQuestion($question)->with('next', $next)->with('answered', true);
            }
        }
        else if ($question->question_type == 'multiple-answer') {

            if (DB::table('user_question')->where('user_id', Auth::id())->where('question_id', $question->id)->value('answered') == false) {

                $answers = $question->answers;
                $good = 0;
                if (count($request->check_list) > 0) {
                    $user_answers = array();

                    foreach ($request->check_list as $choice) {
                        $user_answers[] = $choice;
                    }

                    if (count($user_answers) != count($answers)) {
                        $good = 0;
                        $next++;

                        $incorrect = DB::table('question_stats')->where('question_id', $question->id)->value('incorrect');
                        $incorrect++;
                        DB::table('question_stats')->where('question_id', $question->id)->update(array('incorrect' => $incorrect));

                        return view('courses.tests.check')->withCourse($course)->withTest($test)
                            ->with('next', $next)->with('answers', $answers)->withQuestion($question)
                            ->with('good', $good)->with('answered', false);
                    }

                    $good = 1;

                    foreach ($user_answers as $user_answer) {
                        $temp = 0;
                        foreach ($answers as $answer) {
                            if ($user_answer == $answer['answer']) {
                                $temp = 1;
                            }
                        }
                        if ($temp == 0) {
                            $good = 0;

                            $incorrect = DB::table('question_stats')->where('question_id', $question->id)->value('incorrect');
                            $incorrect++;
                            DB::table('question_stats')->where('question_id', $question->id)->update(array('incorrect' => $incorrect));

                            break;
                        }
                    }

                    if ($good == 1) {
                        $points = DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->value('points');
                        $points++;
                        DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->update(array('points' => $points));

                        $correct = DB::table('question_stats')->where('question_id', $question->id)->value('correct');
                        $correct++;
                        DB::table('question_stats')->where('question_id', $question->id)->update(array('correct' => $correct));

                    }

                } else if (count($request->check_list) == count($answers)) {
                    $good = 1;
                    $points = DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->value('points');
                    $points++;
                    DB::table('user_points')->where('user_id', Auth::id())->where('test_id', $test->id)->update(array('points' => $points));

                    $correct = DB::table('question_stats')->where('question_id', $question->id)->value('correct');
                    $correct++;
                    DB::table('question_stats')->where('question_id', $question->id)->update(array('correct' => $correct));
                }
                $next++;
                return view('courses.tests.check')->withCourse($course)->withTest($test)
                    ->with('next', $next)->with('answers', $answers)->withQuestion($question)
                    ->with('good', $good)->with('answered', false);
            }
            else {
                $next++;
                return view('courses.tests.check')->withCourse($course)->withTest($test)
                    ->withQuestion($question)->with('next', $next)->with('answered', true);
            }
        }
        else if ($question->question_type == 'open') {
            if (DB::table('user_question')->where('user_id', Auth::id())->where('question_id', $question->id)->value('answered') == false) {

                $open_question = new Open_question();
                $open_question->question_id = $question->id;
                $open_question->user_id = Auth::id();
                $open_question->long_answer = $request->choice;
                $open_question->save();

                $next++;
                return view('courses.tests.check')->withCourse($course)->withTest($test)
                    ->with('next', $next)->withQuestion($question)->with('answered', false);
            }
        }
        else {
            $next++;
            return view('courses.tests.check')->withCourse($course)->withTest($test)
                ->withQuestion($question)->with('next', $next)->with('answered', true);
        }
    }

    public function checkOpenQuestion(Request $request, Course $course, Test $test)
    {
        $this->validate($request, [
            'is_correct' => 'required',
            'answer_id' => 'required',
            'question_id' => 'required'
        ]);

        if($request->is_correct == 'false') {

            $question_id = $request->question_id;
            $incorrect = DB::table('question_stats')->where('question_id', $question_id)->value('incorrect');
            $incorrect++;
            DB::table('question_stats')->where('question_id', $question_id)->update(['incorrect' => $incorrect]);

            $answer_id = $request->answer_id;

            Open_question::where('id', $answer_id)->update(['is_correct' => false]);
        }
        else if($request->is_correct == true) {

            $question_id = $request->question_id;
            $correct = DB::table('question_stats')->where('question_id', $question_id)->value('correct');
            $correct++;
            DB::table('question_stats')->where('question_id', $question_id)->update(['correct' => $correct]);

            $answer_id = $request->answer_id;

            $student_id = Open_question::where('id', $answer_id)->value('user_id');
            $points = DB::table('user_points')->where('test_id', $test->id)->where('user_id', $student_id)->value('points');
            $points++;
            DB::table('user_points')->where('test_id', $test->id)->where('user_id', $student_id)->update(['points' => $points]);

            Open_question::where('id', $answer_id)->update(['is_correct' => true]);
        }

        return view('courses.tests.show')->withCourse($course)->withTest($test);
    }

    public function store(Request $request, Course $course)
    {
        $this->validate($request, [
            'name' => 'required|max:255',       //potem zabezpieczyc by testy danego nauczyciela mialy rozne nazwy
            'description' => 'required',
            'release_date' => 'nullable|date',
            'deadline' => 'nullable|date'
        ]);

        $test=new Test();
        $test->name = $request->name;
        $test->description = $request->description;
        $test->release_date = $request->release_date;
        $test->deadline = $request->deadline;
        $course->tests()->save($test);

        //return redirect()->route('courses.tests.edit', [$course, $test]);
        return redirect()->route('courses.tests.edit',[$course,$test]);//->withCourse($course)->withTest($test);

    }


    public function show(Course $course, Test $test)
    {
        return view('courses.tests.show')->withCourse($course)->withTest($test);
    }


    public function edit(Course $course, Test $test)
    {
        return view('courses.tests.edit')->withCourse($course)->withTest($test);
    }


    public function update(Request $request, Course $course, Test $test)
    {
        if(isset($request->description))
            DB::table('tests')->where('id',$test->id)->update(['name' =>$request->name,'description' => $request->description]);
        else
            DB::table('tests')->where('id',$test->id)->update(['deadline'=>$request->deadline,'release_date'=>$request->release_date]);

        return redirect()->route('courses.tests.show',[$course,$test]);
    }


    public function destroy(Course $course, Test $test)
    {
        DB::table('tests')->where('id',$test->id)->delete();
        return view('courses.tests.index')->withCourse($course);
    }
}
