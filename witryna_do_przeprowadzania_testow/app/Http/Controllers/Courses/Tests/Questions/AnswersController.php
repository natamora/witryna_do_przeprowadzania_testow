<?php

namespace App\Http\Controllers\Courses\Tests\Questions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\All_answer;
use App\Correct_answer;
use App\Question;
use App\Course;
use App\Test;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Categorization_answer;

class AnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Course $course, Test $test, Question $question)
    {
        return view('courses.tests.questions.answers.index')->withCourse($course)->withTest($test)->withQuestion($question);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Course $course, Test $test, Question $question)
    {
        return view('courses.tests.questions.answers.create')->withCourse($course)->withTest($test)->withQuestion($question);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Course $course,Test $test,Question $question)
    {


        switch($question->question_type)
        {
            case('single-answer'):
                $validator = Validator::make($request->all(), [
                    'answer' => 'required|max:255',
                ]);

                if ($validator->fails()) {
                    //return redirect()->route('courses.tests.questions.answer.create',[$course,$test,$question])
                    //    ->withErrors($validator)->withInput();
                    return view('courses.tests.questions.answers.create')->withCourse($course)->withTest($test)->withQuestion($question)->withErrors($validator);
                }
                if($request->correct){ //zabezpieczenie przed 2 poprawnymi odpowiedziami
                    if($question->correct_answers_count>0)
                    {
                        $validator->errors()
                            ->add('single', 'Only one correct answer in single-answer');
                        return view('courses.tests.questions.answers.create')->withCourse($course)->withTest($test)->withQuestion($question)->withErrors($validator);
                    }
            }
                $answer = new All_answer();
                $answer->answer = $request->answer;
                $question->all_answers()->save($answer);
                $answer->id = DB::table('all_answers')->max('id');
                $question->answers_count++;
                $test->questions()->save($question);
                if($request->correct) {
                    $answer->questions()->attach($question->id);
                    $question->correct_answers_count++;
                }
                $test->questions()->save($question);
                return view('courses.tests.questions.edit')->withCourse($course)->withTest($test)->withQuestion($question);

                break;
            case('multiple-answer'):
                $validator = Validator::make($request->all(), [
                    'answer' => 'required|max:255',
                ]);

                if ($validator->fails()) {
                    //return redirect()->route('courses.tests.questions.answer.create',[$course,$test,$question])
                    //    ->withErrors($validator)->withInput();
                    return view('courses.tests.questions.answers.create')->withCourse($course)->withTest($test)->withQuestion($question)->withErrors($validator);
                }
                $answer = new All_answer();
                $answer->answer = $request->answer;
                $question->all_answers()->save($answer);
                $answer->id = DB::table('all_answers')->max('id');
                $question->answers_count++;
                $test->questions()->save($question);
                if($request->correct) {
                    $answer->questions()->attach($question->id);
                    $question->correct_answers_count++;
                }
                $test->questions()->save($question);
                return view('courses.tests.questions.edit')->withCourse($course)->withTest($test)->withQuestion($question);

                break;
            case('open'):
                break;
            case('categorization'):
                $validator = Validator::make($request->all(), [
                    'first' => 'required|max:255',
                    'second' =>'required|max:255'
                ]);

                if ($validator->fails()) {
                    //return redirect()->route('courses.tests.questions.answer.create',[$course,$test,$question])
                    //    ->withErrors($validator)->withInput();
                    return view('courses.tests.questions.answers.create')->withCourse($course)->withTest($test)->withQuestion($question)->withErrors($validator);
                }
                $answer=new Categorization_answer();
                $answer->first=$request->first;
                $answer->second=$request->second;
                $question->categorization_answers()->save($answer);
                $answer->id=DB::table('categorization_answers')->max('id');
                $question->answers_count++;
                $test->questions()->save($question);

                return view('courses.tests.questions.edit')->withCourse($course)->withTest($test)->withQuestion($question);

                break;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course, Test $test, Question $question, $answerid)
    {
        return view('courses.tests.questions.answers.edit')
            ->withCourse($course)->withTest($test)->withQuestion($question)->with('answerid',$answerid);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course, Test $test, Question $question, $answerid)
    {
        switch($question->question_type)
        {
            case('single-answer'):
            case('multiple-answer'):
                DB::table('all_answers')->where('id',$answerid)->update(['answer' => $request->answer]);
                break;
            case('categorization'):
                DB::table('categorization_answers')->where('id',$answerid)->update(['first' => $request->first , 'second' => $request->second]);
                break;
        }
        return view('courses.tests.questions.edit')->withCourse($course)->withTest($test)->withQuestion($question);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course, Test $test, Question $question, $answerid)
    {
        switch($question->question_type)
        {
            case('single-answer'):
            case('multiple-answer'):

                if($question->answers()->where('answer_id', $answerid)->exists())
                    $question->correct_answers_count--;
                $question->answers_count--;

                DB::table('questions')->where('id',$question->id)
                    ->update(['answers_count' => $question->answers_count, 'correct_answers_count' => $question->correct_answers_count]);
                DB::table('all_answers')->where('id',$answerid)->delete();


            break;
            case('categorization'):
                $question->answers_count--;
                DB::table('questions')->where('id',$question->id)
                    ->update(['answers_count' => $question->answers_count]);

                DB::table('categorization_answers')->where('id',$answerid)->delete();
                break;
        }
        return view('courses.tests.questions.edit')->withCourse($course)->withTest($test)->withQuestion($question);

    }
}
