<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //pytanie ma wiele odpowiedzi 1:N
    function all_answers()
    {
        return $this->hasMany(All_answer::class );
    }
    //relacja N:M dla poprawnych odpowiedzi pytanie_odpowiedz
    function answers()
    {
        return $this->belongsToMany( 'App\All_answer','correct_answers','question_id','answer_id')->withTimestamps();
    }
    function test()
    {
        return $this->belongsTo('App\Test','test_id');
    }
    function question_stats()
    {
        return $this->hasOne('App\Question_stat');
    }
    function categorization_answers()
    {
        return $this->hasMany(Categorization_answer::class);
    }
    function open_questions()
    {
        if($this->question_type == 'open')
            return $this->hasMany('App\Open_question', 'question_id');
    }
    function users()
    {
        return $this->belongsToMany('App\User', 'user_question', 'question_id','user_id')->withPivot('answered')->withTimestamps();
    }
}
