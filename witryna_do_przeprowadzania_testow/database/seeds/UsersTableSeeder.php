<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        factory(App\User::class, 'student', 7)->create();
        factory(App\User::class, 'teacher', 7)->create();
    }
}
