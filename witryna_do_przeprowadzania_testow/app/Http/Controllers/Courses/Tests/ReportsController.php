<?php

namespace App\Http\Controllers\Courses\Tests;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use App\Course;
use App\Test;

class ReportsController extends Controller
{
    public function download(Course $course, Test $test)
    {
        $data = ['course' => $course, 'test' => $test];

        $pdf = PDF::loadView('courses.tests.reports.report', $data);
        return $pdf->download('raport.pdf');
    }
}