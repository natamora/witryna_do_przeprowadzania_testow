<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('see only available tests');

$student_name = 'Jane Doe';
$student_email = 'jane@doe.com';
$student_password = 'jane123';

$teacher_name = 'Rasmus Lerdorf';
$teacher_email = 'teacher@mail.com';
$teacher_password = 'rsms123';

$course = 'PHP II';
$description = 'advanced programming with PHP';

$I->haveInDatabase('users', [
    'email' => $student_email,
    'name' => $student_name,
    'password' => password_hash($student_password, PASSWORD_DEFAULT),
    'identity' => 'student',
]);

$I->haveInDatabase('users', [
    'email' => $teacher_email,
    'name' => $teacher_name,
    'password' => password_hash($teacher_password, PASSWORD_DEFAULT),
    'identity' => 'teacher',
]);

$student_id = $I->grabFromDatabase('users', 'id', array('email' => $student_email));
$teacher_id = $I->grabFromDatabase('users', 'id', array('email' => $teacher_email));

$I->haveInDatabase('courses', [
    'name' => $course,
    'description' => $description,
    'teacher_id' => $teacher_id,
]);

$course_id = $I->grabFromDatabase('courses', 'id', array('name'=> $course));

$I->haveInDatabase('course_user',[
    'user_id' => $student_id,
    'course_id' => $course_id,
    'confirmed' => true
]);

$old_test_name = 'old test';
$old_test_release = '2005-01-01 00:00';
$old_test_deadline = '2005-11-11 00:00';

$valid_test_name = 'valid test';
$valid_test_release = '2005-01-01 00:00';
$valid_test_deadline = '2050-11-11 00:00';

$not_released_test_name = 'test from future';
$not_released_test_release = '2050-01-01 00:00';
$not_released_test_deadline = '2050-11-11 00:00';

$wrong_test_name = 'test with incorrect dates';
$wrong_test_release = '2050-01-01 00:00';
$wrong_test_deadline = '2005-11-11 00:00';

$null_test_name = 'test with no date';
$some_test_description = 'some description';

$I->haveInDatabase('tests', [
    'name' => $valid_test_name,
    'description' => $some_test_description,
    'course_id' => $course_id,
    'release_date' => $valid_test_release,
    'deadline' => $valid_test_deadline,
]);

$I->haveInDatabase('tests', [
    'name' => $old_test_name,
    'description' => $some_test_description,
    'course_id' => $course_id,
    'release_date' => $old_test_release,
    'deadline' => $old_test_deadline,
]);

$I->haveInDatabase('tests', [
    'name' => $not_released_test_name,
    'description' => $some_test_description,
    'course_id' => $course_id,
    'release_date' => $not_released_test_release,
    'deadline' => $not_released_test_deadline,
]);

$I->haveInDatabase('tests', [
    'name' => $wrong_test_name,
    'description' => $some_test_description,
    'course_id' => $course_id,
    'release_date' => $wrong_test_release,
    'deadline' => $wrong_test_deadline,
]);

$I->haveInDatabase('tests', [
    'name' => $null_test_name,
    'description' => $some_test_description,
    'course_id' => $course_id,
]);

$valid_test_id = $I->grabFromDatabase('tests', 'id', array('name' => $valid_test_name));
$old_test_id = $I->grabFromDatabase('tests', 'id', array('name' => $old_test_name));
$not_released_test_id = $I->grabFromDatabase('tests', 'id', array('name' => $not_released_test_name));
$wrong_test_id = $I->grabFromDatabase('tests', 'id', array('name' => $wrong_test_name));
$null_test_id = $I->grabFromDatabase('tests', 'id', array('name' => $null_test_name));


$I->seeInDatabase('tests',[
    'id' => $valid_test_id,
],[
    'id' => $old_test_id,
],[
    'id' => $not_released_test_id,
],[
    'id' => $wrong_test_id,
],[
    'id' => $null_test_id,
]);


$I->amOnPage('/login');
$I->fillField('email', $student_email);
$I->fillField('password', $student_password);
$I->click('button[type=submit]');

$I->amOnPage('/courses/' . $course_id);
$I->see($valid_test_name);

$I->dontSee($old_test_name);
$I->dontSee($not_released_test_name);
$I->dontSee($wrong_test_name);
$I->dontSee($null_test_name);