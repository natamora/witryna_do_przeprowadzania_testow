<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('solve a test as a student');

$student_name = 'Jane Doe';
$student_email = 'jane@doe.com';
$student_password = 'jane123';

$teacher_name = 'Rasmus Lerdorf';
$teacher_email = 'teacher@mail.com';
$teacher_password = 'rsms123';

$course = 'PHP II';
$description = 'advanced programming with PHP';

$test_name = 'some test';
$test_release = '2005-01-01 00:00';
$test_deadline = '2050-11-11 00:00';
$test_description = 'some description';

$question1 = 'Describe something';
$question2 = 'Describe another thing';
$question3 = 'Tell me 3 differences between those things';

$I->haveInDatabase('users', [
    'email' => $student_email,
    'name' => $student_name,
    'password' => password_hash($student_password, PASSWORD_DEFAULT),
    'identity' => 'student',
]);

$I->haveInDatabase('users', [
    'email' => $teacher_email,
    'name' => $teacher_name,
    'password' => password_hash($teacher_password, PASSWORD_DEFAULT),
    'identity' => 'teacher',
]);

$student_id = $I->grabFromDatabase('users', 'id', array('email' => $student_email));
$teacher_id = $I->grabFromDatabase('users', 'id', array('email' => $teacher_email));

$I->haveInDatabase('courses', [
    'name' => $course,
    'description' => $description,
    'teacher_id' => $teacher_id,
]);

$course_id = $I->grabFromDatabase('courses', 'id', array('name'=> $course));

$I->haveInDatabase('course_user',[
    'user_id' => $student_id,
    'course_id' => $course_id,
    'confirmed' => true
]);

$I->haveInDatabase('tests', [
    'name' => $test_name,
    'description' => $test_description,
    'course_id' => $course_id,
    'release_date' => $test_release,
    'deadline' => $test_deadline,
]);

$test_id = $I->grabFromDatabase('tests', 'id', array('name' => $test_name));

$I->haveInDatabase('questions',[
    'test_id' => $test_id,
    'content'=> $question1,
    'question_type' => 'open',
]);

$I->haveInDatabase('questions',[
    'test_id' => $test_id,
    'content'=> $question2,
    'question_type' => 'open',
]);

$I->haveInDatabase('questions',[
    'test_id' => $test_id,
    'content'=> $question3,
    'question_type' => 'open',
]);

$answer1 = 'thing';
$answer2 = 'anything';
$answer3 = 'something';

$I->amOnPage('/login');
$I->fillField('email', $student_email);
$I->fillField('password', $student_password);
$I->click('button[type=submit]');

$I->amOnPage('/courses');
$I->click($course);
$I->click('Solve');
$I->click('Solve Now!');

$I->fillField('choice', $answer1);
$I->click('Next question');
$I->see('This question needs to be verified by teacher. ');
$I->click('Next!');

$I->fillField('choice', $answer2);
$I->click('Next question');
$I->see('This question needs to be verified by teacher. ');
$I->click('Next!');

$I->fillField('choice', $answer3);
$I->click('Next question');
$I->see('This question needs to be verified by teacher. ');
$I->click('Next!');

$I->see('Thanks for taking the test Your results: 0 / 3 ');

$I->seeInDatabase('open_questions',[
    'user_id' => $student_id,
    'long_answer' => $answer1,
    'is_correct' => null
],[
    'user_id' => $student_id,
    'long_answer' => $answer2,
    'is_correct' => null
],[
    'user_id' => $student_id,
    'long_answer' => $answer3,
    'is_correct' => null
]);