<?php

namespace App\Http\Controllers\Courses\Tests;
use Illuminate\Support\Facades\DB;
use App\All_answer;
use App\Correct_answer;
use App\Question_stat;
use App\Question;
use App\Course;
use App\Test;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class QuestionsController extends Controller
{

    public function index(Course $course,Test $test)
    {
        return view('courses.tests.questions.index')->withCourse($course)->withTest($test);
    }

    public function choose(Request $request, Course $course, Test $test){

        //if($request->method()=='post')

            return view('courses.tests.questions.create')->withCourse($course)->withTest($test)->with('choice',$request->question_type);
        //else return view('courses.tests.questions.create')->withCourse($course)->withTest($test)->with('choice',$request->question_type);
    }

    public function create(Course $course,Test $test)
    {
        return view('courses.tests.questions.create')->withCourse($course)->withTest($test);//->with('question_type',$request->input('question_type'));
    }


    public function getstore(Request $request, Course $course, Test $test, $choice){
//        $this->validate($request, [
//            'question' => 'required|max:255',
//        ]);
        $validator = Validator::make($request->all(), [
            'question' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->route('courses.tests.questions.index',[$course,$test])
                ->withErrors($validator)->withInput();
        }

        switch($choice){
            case 'single-answer':
                $type=1;
                break;
            case 'multiple-answer':
                $type=2;
                break;
            case 'open':
                $type=3;
                break;
            case 'categorization':
                $type=4;
        }


        $question=new Question();
        $question->content=$request->question;
        $question->question_type=$type;
        $question->correct_answers_count=0;
        $question->answers_count=0;
        $test->questions()->save($question);
        $question->id= DB::table('questions')->max('id');

        $question_stat = new Question_stat();
        $question_stat->correct = 0;
        $question_stat->incorrect = 0;
        $question->question_stats()->save($question_stat);
        
        if($type==3)
            return redirect()->route('courses.tests.edit',[$course,$test]);

        //return view('courses.tests.questions.answers.create')->withCourse($course)->withTest($test)->withQuestion($question);
        return redirect()->route('courses.tests.questions.edit',[$course,$test,$question]);
    }

    public function store(Request $request, Course $course, Test $test)
    {

        $this->validate($request, [
            'question' => 'required|max:255',
        ]);

        $question=new Question();
        $question->content=$request->question;
        $question->question_type=1;
        $test->questions()->save($question);
        $question->id= DB::table('questions')->max('id');

        return view('courses.tests.questions.answers.index')->withCourse($course)->withTest($test)->withQuestion($question);
        //return redirect()->route('courses.tests.edit', [$course, $test]);
    }

    public function show(Question $question)
    {
        //
    }

    public function edit(Course $course, Test $test, Question $question)
    {
        return view('courses.tests.questions.edit')->withCourse($course)->withTest($test)->withQuestion($question);
    }

    public function update(Request $request, Course $course, Test $test, Question $question)
    {
        DB::table('questions')->where('id',$question->id)->update(['content' => $request->question]);
        return view('courses.tests.edit')->withCourse($course)->withTest($test);

    }

    public function destroy(Course $course, Test $test, Question $question)
    {
        switch($question->question_type)
        {
            case('single-answer'):
            case('multiple-answer'):
                DB::table('questions')->where('id',$question->id)->delete();

                //DB::table('all_answers')->where('id',$answerid)->delete();


                break;
            case('categorization'):

                DB::table('questions')->where('id',$question->id)
                    ->delete();

        }
        return view('courses.tests.edit')->withCourse($course)->withTest($test);

    }
}
