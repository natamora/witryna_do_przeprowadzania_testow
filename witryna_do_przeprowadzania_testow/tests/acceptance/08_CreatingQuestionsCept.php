<?php
$I = new AcceptanceTester($scenario);
$I->wantTo('create questions in an existing test');

$student_name = 'Jane Doe';
$student_email = 'jane@doe.com';
$student_password = 'jane123';

$teacher_name = 'Rasmus Lerdorf';
$teacher_email = 'teacher@mail.com';
$teacher_password = 'rsms123';

$course = 'PHP II';
$description = 'advanced programming with PHP';

$I->haveInDatabase('users', [
    'email' => $student_email,
    'name' => $student_name,
    'password' => password_hash($student_password, PASSWORD_DEFAULT),
    'identity' => 'student',
]);

$I->haveInDatabase('users', [
    'email' => $teacher_email,
    'name' => $teacher_name,
    'password' => password_hash($teacher_password, PASSWORD_DEFAULT),
    'identity' => 'teacher',
]);

$student_id = $I->grabFromDatabase('users', 'id', array('email' => $student_email));
$teacher_id = $I->grabFromDatabase('users', 'id', array('email' => $teacher_email));

$I->haveInDatabase('courses', [
    'name' => $course,
    'description' => $description,
    'teacher_id' => $teacher_id,
]);

$course_id = $I->grabFromDatabase('courses', 'id', array('name'=> $course));

$I->haveInDatabase('course_user',[
    'user_id' => $student_id,
    'course_id' => $course_id,
    'confirmed' => true
]);

$test_name = 'some test';
$test_release = '2005-01-01 00:00';
$test_deadline = '2050-11-11 00:00';
$test_description = 'some description';

$I->haveInDatabase('tests', [
    'name' => $test_name,
    'description' => $test_description,
    'course_id' => $course_id,
    'release_date' => $test_release,
    'deadline' => $test_deadline,
]);

$test_id = $I->grabFromDatabase('tests', 'id', array('name' => $test_name));

$question = 'Some question very difficult to answer';
$answer1 = '40';
$answer2 = '41';
$answer3 = '42';
$answer4 = '43';

$I->dontSeeInDatabase('questions',[
    'test_id' => $test_id,
    'content'=> $question,
    'answers_count' => 4,
    'correct_answers_count' => 1,
    'question_type' => 'Single-answer question',

]);

$I->amOnPage('/login');
$I->fillField('email', $teacher_email);
$I->fillField('password', $teacher_password);
$I->click('button[type=submit]');

$I->amOnPage('/courses/' . $course_id . '/tests/' . $test_id);
$I->seeLink('Edit test...');
$I->click('Edit test...');

$I->seeInCurrentUrl('/courses/' . $course_id . '/tests/' . $test_id . '/edit');
$I->see($test_name);
$I->see($test_description);
$I->see('Create question');
$I->click('Create question');

$I->seeInCurrentUrl('/courses/' . $course_id . '/tests/' . $test_id . '/questions');
$I->see('Single-answer question');
$I->see('Multiple-answer question');
$I->see('Open question');
$I->see('Categorization question');
$I->selectOption('question_type', 'Single-answer question');
$I->click('Create question');

$I->fillField('question',$question);
$I->click('Create and add answers');

$I->click('Add answer');
$I->fillField('answer', $answer1);
$I->click('Add answer');
$I->see($answer1);

$I->click('Add answer');
$I->fillField('answer', $answer2);
$I->click('Add answer');
$I->see($answer1);
$I->see($answer2);

$I->click('Add answer');
$I->fillField('answer', $answer3);
$I->checkOption('correct');
$I->click('Add answer');
$I->see($answer1);
$I->see($answer2);
$I->see($answer3);

$I->click('Add answer');
$I->fillField('answer', $answer4);
$I->click('Add answer');
$I->see($answer1);
$I->see($answer2);
$I->see($answer3);
$I->see($answer4);

$I->click('Save question');

$I->seeInDatabase('questions',[
    'test_id' => $test_id,
    'content'=> $question,
    'answers_count' => 4,
    'correct_answers_count' => 1
]);

$question_id = $I->grabFromDatabase('questions', 'id', array('test_id'=> $test_id, 'content'=>$question));

$I->seeInDatabase('all_answers',[
    'question_id' => $question_id,
    'answer' => $answer1,
],[
    'question_id' => $question_id,
    'answer' => $answer2,
],[
    'question_id' => $question_id,
    'answer' => $answer3,
],[
    'question_id' => $question_id,
    'answer' => $answer4,
]);

$correct_id = $I->grabFromDatabase('all_answers', 'id', array('question_id' => $question_id, 'answer' => $answer3));

$I->seeInDatabase('correct_answers',[
    'question_id' => $question_id,
    'answer_id' =>$correct_id
]);
