<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'identity',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    function courses()
    {
        if($this->identity == 'teacher')
            return $this->hasMany('App\Course', "teacher_id");

        if($this->identity == 'student')
            return $this->belongsToMany('App\Course')->withPivot('confirmed')->withTimestamps();
    }
    function tests()
    {
        return $this->belongsToMany( 'App\Test','user_points','user_id','test_id')
            ->withPivot('points')->withTimestamps();
    }
    function open_questions()
    {
        if($this->identity == 'student')
            return $this->hasMany('App\Open_question', "user_id");
    }
    function questions()
    {
        return $this->belongsToMany('App\Question', 'user_question', 'user_id','question_id')->withPivot('answered')->withTimestamps();
    }
}
