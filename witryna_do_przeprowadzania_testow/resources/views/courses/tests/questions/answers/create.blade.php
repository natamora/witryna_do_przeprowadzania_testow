@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="bg-primary text-white">
                        <div class="panel-heading">Create answer</div>
                    </div>
                    <div class="panel-body">
                        @if (Auth::user()->identity == "teacher")
                            Page to create answer <br/><br/>

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @switch($question->question_type)
                                @case(1)
                                @case('single-answer')
                                @case(2)
                                @case('multiple-answer')
                                    <form class="form-horizontal" method="post" action="{{ route('courses.tests.questions.answers.store', [$course,$test,$question]) }}">
                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label for="answer" class="col-md-4 control-label">Answer: </label>
                                            <div class="col-md-6">
                                                <textarea id='answer' class="form-control" type="text" name="answer"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input  type="checkbox" name="correct" value="correct"> <b>Correct</b>
                                                </label>

                                            </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    Add answer
                                                </button>
                                            </div>
                                        </div>
                                    </form>


                                @break;

                                @case(4)
                                @case('categorization')
                                <form class="form-horizontal" method="post" action="{{ route('courses.tests.questions.answers.store', [$course,$test,$question]) }}">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label for="first" class="col-md-4 control-label">First: </label>
                                        <div class="col-md-6">
                                            <input id='first' class="form-control" type="text" name="first"></input>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="second" class="col-md-4 control-label">Second: </label>
                                        <div class="col-md-6">
                                            <input id='second' class="form-control" type="text" name="second">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Add answer
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                @break;
                                @default


                                @endswitch
                            <a href="{{route('courses.tests.edit',[$course,$test])}}">Zakoncz dodawanie odpowiedzi</a>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
