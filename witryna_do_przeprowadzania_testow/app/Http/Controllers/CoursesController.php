<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::all();

        return view('courses.index')->withCourses($courses);
    }

    public function create()
    {
        return view('courses.create');
    }

    public function apply(Request $request)
    {
        $courses = Course::all()->whereNotIn('id', $request->user()->courses()->get()->pluck('id'));

        return view('courses.apply')->withCourses($courses);
    }

    public function show(Course $course)
    {
        if ($course->teacher_id == Auth::id()) {
            return view('courses.show')->withCourse($course);
        }
        else if(DB::table('course_user')->where('user_id', Auth::id())->value('user_id')  == Auth::id()){
            return view('courses.show')->withCourse($course);
        }
        else {
            $courses = Course::all();
            return view('courses.index')->withCourses($courses);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'id' => 'required'.$request->user(),
        ]);

        $course = new Course();
        $course->name = $request->name;
        $course->description = $request->description;
        $course->teacher_id = $request->user()->id;
        $course->save();

        $courses = Course::all();

        return redirect()->route('courses.index');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'course' => 'required',
            'id' => 'required'.$request->user(),
        ]);

        Course::find($request->course)->users()->attach($request->user()->id);

        return redirect()->route('courses.index');
    }

    public function confirm(Request $request, Course $course)
    {
        $this->validate($request, [
            'confirmation' => 'required',
            'user_id' =>'required',
        ]);

        if($request->confirmation == 'false')
            Course::find($course->id)->users()->detach($request->user_id);

        if($request->confirmation == 'true')
            Course::find($course->id)->users()->updateExistingPivot($request->user_id, ['confirmed' => true]);

        return view('courses.show')->withCourse($course);
    }
}
